/**
 * Copyright 2016 Nigel Schuster.
 */


package edu.lawrence.rest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


/**
 * @author nschuste
 * @version 1.0.0
 * @since Feb 5, 2016
 */
@RunWith(MockitoJUnitRunner.class)
public class HelloWorldRestController_helloWorld_Test {
  @InjectMocks
  private HelloWorldRestController controller;
  private MockMvc mockMvc;

  /**
   * @author nschuste
   * @version 1.0.0
   * @throws java.lang.Exception
   * @since Feb 5, 2016
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  /**
   * @author nschuste
   * @version 1.0.0
   * @throws java.lang.Exception
   * @since Feb 5, 2016
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @author nschuste
   * @version 1.0.0
   * @throws java.lang.Exception
   * @since Feb 5, 2016
   */
  @Before
  public void setUp() throws Exception {
    this.mockMvc = MockMvcBuilders.standaloneSetup(this.controller).build();
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @throws java.lang.Exception
   * @since Feb 5, 2016
   */
  @After
  public void tearDown() throws Exception {}

  @Test
  public final void test() throws Exception {
    this.mockMvc.perform(MockMvcRequestBuilders.get("/hello/World"))
        .andExpect(MockMvcResultMatchers.content().json("{\"text\": \"Hello, World\"}"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

}
