/**
 * Copyright 2016 Nigel Schuster.
 */


package edu.lawrence.data;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.lawrence.to.SimpleTo;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Feb 5, 2016
 */
public interface HelloWorldRepository extends JpaRepository<SimpleTo, Long> {

}
