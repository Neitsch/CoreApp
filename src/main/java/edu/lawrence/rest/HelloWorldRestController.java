/**
 * Copyright 2016 Nigel Schuster.
 */


package edu.lawrence.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.lawrence.data.HelloWorldRepository;
import edu.lawrence.to.SimpleTo;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Feb 5, 2016
 */
@RequestMapping("/hello")
@RestController
public class HelloWorldRestController {
  @Autowired
  private HelloWorldRepository repository;

  @ResponseBody
  @RequestMapping("/id/{id}")
  public SimpleTo helloWorld(@PathVariable("id") final Long id) {
    return this.repository.findOne(id);
  }

  @RequestMapping("/save/{txt}")
  public Long save(@PathVariable("txt") final String text) {
    final SimpleTo to = new SimpleTo();
    to.setText(text);
    return this.repository.save(to).getId();
  }
}
