/**
 * Copyright 2016 Nigel Schuster.
 */


package edu.lawrence.to;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author nschuste
 * @version 1.0.0
 * @since Feb 5, 2016
 */
@Entity
public class SimpleTo {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String text;

  /**
   * Gets value for id.
   * 
   * @author nschuste
   * @version 1.0.0
   * @return id of type {@link Long}
   * @since Feb 5, 2016
   */
  public final Long getId() {
    return this.id;
  }

  /**
   * Gets value for text.
   *
   * @author nschuste
   * @version 1.0.0
   * @return text of type {@link String}
   * @since Feb 5, 2016
   */
  public final String getText() {
    return this.text;
  }

  /**
   * Sets value for id.
   * 
   * @author nschuste
   * @version 1.0.0
   * @param id id to set of type {@link Long}
   * @since Feb 5, 2016
   */
  public final void setId(final Long id) {
    this.id = id;
  }

  /**
   * Sets value for text.
   *
   * @author nschuste
   * @version 1.0.0
   * @param text text to set of type {@link String}
   * @since Feb 5, 2016
   */
  public final void setText(final String text) {
    this.text = text;
  }
}
