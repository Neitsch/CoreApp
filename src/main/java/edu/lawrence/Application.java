/**
 * Copyright 2016 Nigel Schuster.
 */


package edu.lawrence;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Feb 5, 2016
 */
@EnableJpaRepositories(basePackages = "edu.lawrence")
@ComponentScan(basePackages = "edu.lawrence")
@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
public class Application implements CommandLineRunner {
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
   * @since Feb 5, 2016
   */
  @Override
  public void run(final String... args) throws Exception {}
}
